obj-m += firewall.o
PWD = $(shell pwd)

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
	# insmod firewall.ko
	# make clean

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
