#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/version.h>
#include <linux/netfilter/x_tables.h>
#include <linux/skbuff.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/udp.h>
#include <linux/if_ether.h>
#include <linux/rbtree.h>
#include <linux/kref.h>
#include <linux/time.h>
#include <linux/inet.h>
#include <linux/string.h>

#include <net/netfilter/nf_conntrack.h>
#include <net/netfilter/nf_conntrack_ecache.h>

#include <linux/slab.h>
#include <linux/fs.h>
#include <asm/uaccess.h>

#define DRIVER_AUTHOR "NAIRITYA KHILARI <nkmann2@gmail.com>"
#define DRIVER_DESC "A Basic Firewall."

#define max_counter 999999
#define max_list 1016
#define static_ip_file "list.txt"
#define iplength 16

MODULE_LICENSE("GPL");
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);

MODULE_SUPPORTED_DEVICE("UBUNTU 14.04");

struct nf_hook_ops nfho;
static __be32 *buff;
static unsigned int buffsize;
static unsigned int Counter = 0;
#include "header/liwall.h"


int init_module(){
    if(start_liwall()){
        nfho.hook = (hook_function_incoming);
        nfho.hooknum = NF_INET_PRE_ROUTING;
        nfho.pf = PF_INET;
        nfho.priority = NF_IP_PRI_FIRST;
        nf_register_hook(&nfho);
        printk(KERN_INFO "simple firewall loaded by nkman\n");
        return 0;
    }
    else{
        printk(KERN_INFO "Not loading !! Errors encountered.\n");
        return -1;
    }
}

void cleanup_module()
{
    printk(KERN_INFO "Removing This Firewall.\n");
    deallocate_func();
    nf_unregister_hook(&nfho);
}