#ifndef _HEADER_LIWALL_H
#define _HEADER_LIWALL_H

#include "sort.h"

static void reset_counter(void);
static int init_buff(void);
static int start_liwall(void);
static unsigned int insertion(void);

void concatenate(char *, char , int);
static void deallocate_func(void);

void reinitialize(char *);

static void print_buff(void);
static unsigned int search(__be32 );

static int start_liwall(void)
{
    if(init_buff()){
        insertion();
        sorted(buff, 0, buffsize-1);
        return 1;
    }
    return 0;
}


static void print_buff()
{
    int i=0;
    for(i=0;i<buffsize;i++)
        printk(KERN_INFO "buff[%d] is %u\n", i, buff[i]);
    return;
}

static int init_buff(void)
{
    int i;
    buff = kmalloc(sizeof(unsigned int *)*max_list, GFP_KERNEL);
    if(buff == NULL){
        printk(KERN_ALERT "Error allocating size to buff. So not loading.");
        return 0;
    }
    else{
        for(i=0;i<max_list;i++)
            buff[i] = 0;
        // memset(buff, 0, sizeof(buff));
        return 1;
    }
}

static unsigned int insertion()
{
    struct file *f;
    mm_segment_t fs;
    char *buf, *temp_buff;
    int i,cursize;
    buffsize = cursize = 0;

    buf = kmalloc(sizeof(char)*max_list, GFP_KERNEL);
    temp_buff = kmalloc(sizeof(char)*iplength, GFP_KERNEL);
    // memset(buf, 0, sizeof(*buf));
    // memset(temp_buff, 0, sizeof(*temp_buff));
    for(i=0;i<max_list;i++){
        buf[i] = 0;
    }

    for(i=0;i<iplength;i++){
        temp_buff[i] = 0;
    }

    f = filp_open(static_ip_file, O_RDONLY, 0);

    if(f == NULL){
        printk(KERN_ALERT "Ha Unable to open File.\n");
        return 0;
    }
    fs = get_fs();
    set_fs(get_ds());
    f->f_op->read(f, buf, max_list, &f->f_pos);
    set_fs(fs);

    filp_close(f,NULL);
    i = 0;
    while(buf[i]){
        if(buf[i] == '\n'){
            buff[buffsize] = in_aton(temp_buff);
            buffsize++;
            cursize=0;
            reinitialize(temp_buff);
        }
        else{
            concatenate(temp_buff, buf[i], cursize);
            cursize++;
        }
        i++;
    }
    kfree(buf);
    kfree(temp_buff);
    print_buff();
    return 1;
}

void reinitialize(char *temp_buff)
{
    int i;
    for(i=0;i<iplength;i++)
        temp_buff[i] = 0;
    // memset(temp_buff, 0, sizeof(temp_buff));
}

void concatenate(char *(dest), char sour, int cursize)
{
    dest[cursize] = sour;
    return;
}

static unsigned int hook_function_incoming(unsigned int hooknum, 
                                            struct sk_buff *skb, 
                                            const struct net_device *in, 
                                            const struct net_device *out, 
                                            int (*okfn)(struct sk_buff *))
{
    struct iphdr *iph = NULL;
    if(skb != NULL){
        iph = ip_hdr((skb));
        Counter++;
        if(Counter > max_counter)
            reset_counter();

        if(iph->protocol == IPPROTO_ICMP){
            char * ipp = "127.0.0.1";
            if(iph->saddr == in_aton(ipp))
                return NF_ACCEPT;
            else
                printk(KERN_INFO "Blocked ICMP Request from %pI4\n", &(iph->saddr));
            return NF_DROP;
        }
        return search(iph->saddr);
    }
    else
        return NF_DROP;
}

static unsigned int search(__be32 addr)
{
    int first, last, middle;
    first = 0; 
    last = buffsize-1; 
    middle = (first+last)/2;
    while(first <= last){
        if(addr == buff[middle]){
            printk(KERN_INFO "Blocked %pI4\n", &(addr));
            return NF_DROP;
        }
        else if(addr > buff[middle])
            first = middle + 1;
        else
            last = middle - 1;
        middle = (first+last)/2;
    }
    printk(KERN_INFO "Accepting %pI4\n", &(addr));
    return NF_ACCEPT;
}


static void reset_counter(void)
{
    Counter = 0;
}

static void deallocate_func(void)
{
    kfree(buff);
    printk(KERN_INFO "Freed buff !\n");
    return;
}

#endif /* _HEADER_LIWALL_H */